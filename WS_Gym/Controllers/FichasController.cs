﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WS_Gym.Models;

namespace WS_Gym.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class FichasController : Controller
    {
        // GET api/Clientes
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return await ListarFicha();
        }

        public async Task<IActionResult> ListarFicha()
        {
           FichaModel fichas = new FichaModel();
            return Ok(fichas.GetFichaModel());
        }
    }
}
