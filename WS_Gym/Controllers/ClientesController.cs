﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WS_Gym.Models;

namespace WS_Gym.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ClientesController : Controller
    {
        // GET api/Clientes
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return await ListarModalidade();
        }

        public async Task<IActionResult> ListarModalidade()
        {
            ClientesModel clientes = new ClientesModel();
            return Ok(clientes.GetClientesModel());
        }
    }
}
