﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WS_Gym.Models
{
    public class ClientesModel
    {
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Modalidade { get; set; }

        public ClientesModel GetClientesModel()
        {
            ClientesModel cliente = new ClientesModel();
            cliente.Nome = "Pedro";
            cliente.Cpf = "123456";
            cliente.Modalidade = "Natação";

            return cliente;
        }

    }


}
