﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WS_Gym.Models
{
    public class FichaModel
    {
        public string NomeExercicio { get; set; }
        public string QtdRepeticoes { get; set; }
        public string Peso { get; set; }
        public string UrlImagem { get; set; }
        public string ValorAcao { get; set; }


        public List<FichaModel> GetFichaModel()
        {
            List<FichaModel> fichas = new List<FichaModel>();

            FichaModel ficha1 = new FichaModel();
            ficha1.NomeExercicio = "Abdominal com elevação de pernas"; 
            ficha1.QtdRepeticoes = "3 Repetições x10 \n\n 1 Minuto de descanso";
            ficha1.Peso = "";
            ficha1.UrlImagem = "http://4.bp.blogspot.com/-BGLUt0Q13NA/UF-GOvFNJ5I/AAAAAAAAAg0/4VGHrp_GU2g/s320/exercicios_abdominais.jpg";
            ficha1.ValorAcao = "http://4.bp.blogspot.com/-BGLUt0Q13NA/UF-GOvFNJ5I/AAAAAAAAAg0/4VGHrp_GU2g/s320/exercicios_abdominais.jpg";

            FichaModel ficha2 = new FichaModel();
            ficha2.NomeExercicio = "Extensão Triceps";
            ficha2.QtdRepeticoes = "2 Repetições x10\n\n 1 Minuto de descanso";
            ficha2.Peso = "";
            ficha2.UrlImagem = "http://3.bp.blogspot.com/-ui_ER4Mj2ww/UF-ItJpnxLI/AAAAAAAAAg8/0wUhBu90HOI/s1600/extensao_do_triceps_deitado.jpg";
            ficha2.ValorAcao = "http://3.bp.blogspot.com/-ui_ER4Mj2ww/UF-ItJpnxLI/AAAAAAAAAg8/0wUhBu90HOI/s1600/extensao_do_triceps_deitado.jpg";

            fichas.Add(ficha1);
            fichas.Add(ficha2);

            return fichas;
        }
    }

}
