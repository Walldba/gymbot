﻿using Microsoft.Bot.Builder.FormFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace poc_architecture_netfram_v2.Models
{
    public enum TecladoTypeService
    {
        [Describe("Teclado nao funciona")]
        TecladoNaoFunciona = 1,
        [Describe("Solicitar novo Teclado")]
        SolicitarNovoTeclado = 2,
        [Describe("Nao encontro meu Teclado")]
        NaoEncontroTeclado = 3,
        [Describe("Voltar as opcoes de Hardware")]
        VoltarOpcoesHardware = 4
    }
}