﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using poc_architecture_netfram_v2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace poc_architecture_netfram_v2.Dialogs.Interfaces
{
    public interface IBaseDialogCard : IDialog<DialogResponse>
    {
        Task MessageReceivedAsyncCardAction(IDialogContext context, IAwaitable<IMessageActivity> result);
        Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result);
        Task MessageResumeAfter(IDialogContext context, IAwaitable<DialogResponse> result);
        //Task MessageResumeAfter(IDialogContext context, IAwaitable<IMessageActivity> result);
        IList<Attachment> GetCardsAttachments();
    }
}
