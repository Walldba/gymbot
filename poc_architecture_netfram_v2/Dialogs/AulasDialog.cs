﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using poc_architecture_netfram_v2.Dialogs.Interfaces;
using poc_architecture_netfram_v2.Models;
using poc_architecture_netfram_v2.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace poc_architecture_netfram_v2.Dialogs
{
    [Serializable]
    public class AulasDialog : IBaseDialogCard
    {
        public const string PodeSimOption = "Pode sim 😎";
        public const string NaoQueroOption = "Não, obrigado.";

        public async Task StartAsync(IDialogContext context)
        {
            ClientesAPIServices _clientes = new ClientesAPIServices();
            List<Clientes> listaClientes = new List<Clientes>();

            listaClientes = _clientes.ListarModalidade();

            foreach (var item in listaClientes)
            {
                await context.PostAsync($"Vi no sistema que você está matriculado na aula de  {item.Modalidade}");
                Thread.Sleep(1000);
                await context.PostAsync($"Está aula será às 14h na piscina principal. O professor Marcelo estará te aguardando!");
            }

            PromptDialog.Choice(
           context,
           this.AfterChoiceSelected,
           new[] { PodeSimOption, NaoQueroOption },
           "Posso te ajudar com mais alguma coisa?",
           "Me desculpe, mas eu preciso que você responda se eu posso ou não te ajudar com mais alguma coisa:",
           attempts: 2);
        }

        public async Task AfterChoiceSelected(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                var selection = await result;
                switch (selection)
                {
                    case PodeSimOption:
                        context.Done(true);
                        break;

                    case NaoQueroOption:
                        await context.PostAsync("Foi ótimo conversar com você 😎✌ \n\n Até a próxima!");
                        context.Call(new GreetingDialog(), null);
                        break;

                }
            }
            catch (TooManyAttemptsException)
            {
                await this.StartAsync(context);
            }

        }

        public IList<Attachment> GetCardsAttachments()
        {
            throw new NotImplementedException();
        }

        public Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            throw new NotImplementedException();
        }

        public Task MessageReceivedAsyncCardAction(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            throw new NotImplementedException();
        }

        public async Task MessageResumeAfter(IDialogContext context, IAwaitable<DialogResponse> result)
        {
            context.Done(true);
        }


    }
}