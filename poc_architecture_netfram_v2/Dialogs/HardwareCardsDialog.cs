﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using poc_architecture_netfram_v2.Dialogs.Interfaces;
using poc_architecture_netfram_v2.Models;
using poc_architecture_netfram_v2.Models.Cards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace poc_architecture_netfram_v2.Dialogs
{
    [Serializable]
    public class HardwareCardsDialog : IBaseDialogCard
    {
        #region Constantes
        //Mouse
        //private const string MouseNaoFuncionaOption = "Mouse não funciona";
        //private const string SolicitarNovoMouseOption = "Solicitar novo Mouse";
        //private const string MouseSumiuOption = "Não encontro meu Mouse";
        //private const string DigiteMouseOption = "Digite seu problema com o Mouse";

        //Teclado
        private const string TecladoNaoFuncionaOption = "Teclado não funciona";
        private const string SolicitarNovoTecladoOption = "Solicitar novo Teclado";
        private const string TecladoSumiuOption = "Não encontro meu Teclado";
        //private const string DigiteTecladoOption = "Digite seu problema com o Teclado";

        //Headeset
        private const string HeadsetNaoFuncionaOption = "Headset não funciona";
        private const string RamalFisicoOption = "Ramal Físico";
        private const string SoftPhoneOption = "SoftPhone";
        private const string RamalFisicoMudoOption = "Ramal Físico está mudo";
        private const string SoftPhoneMudoOption = "SoftPhone está mudo";
        //private const string HeadSetChiandoOption = "HeadSet está chiando";
        private const string HeadSetMudoOption = "Headset Mudo";
        //private const string DigiteHeadSetOption = "Digite seu problema com o Headset";

        //Monitor
        private const string MonitorNaoLigaOption = "Monitor não liga";
        private const string IconesGrandesOption = "Os icones e letras estão grandes";
        private const string CoresDesconfiguradasOption = "As cores estão desconfiguradas";
        //private const string DigiteMonitorOption = "Digite seu problema com o Monitor";

        //Impressora
        private const string NaoSaiuOption = "Mandei imprimir, mas não saiu impressão.";
        private const string ImpressoraTravouOption = "Impressora travou";
        private const string MovimentarImpressoraOption = "Quero mudar impressora de lugar";
        private const string FolhaPresaOption = "Folha está presa";
        private const string AcessoImpressoraOption = "Quero acesso a impressora";
        private const string ImpressoraNaoFuncionaOption = "Impressora não está funcionando";

        //Opções
        public const string PodeSimOption = "Pode sim =D";
        public const string NaoQueroOption = "Não, obrigado.";
        public const string OptionSim = "Sim";
        public const string OptionNao = "Não";
        private const string VoltarMenuHardware = "Voltar as opções Hardware";

        #endregion

        private readonly IBaseDialogForm<OpcoesDialogService> opcoesMouseDialog;

        public HardwareCardsDialog(IBaseDialogForm<OpcoesDialogService> opcoesMouseDialog)
        {
            this.opcoesMouseDialog = opcoesMouseDialog;
        }


        public async Task StartAsync(IDialogContext context)
        {
            var reply = context.MakeMessage();
            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            reply.Attachments = GetCardsAttachments();

            await context.PostAsync(reply);
            context.Wait(this.MessageReceivedAsyncCardAction);
        }

        public IList<Attachment> GetCardsAttachments()
        {
            CardModel card = new CardModel();

            return new List<Attachment>()
            {
                 card.GetThumbnailCard(
                    "Voltar ao Menu",
                    "Não encontrou seu problema? Volte ao Menu :D",
                    "",
                    new CardImage(url: "http://sustainablearchaeology.org/img/backtomenu.png"),
                    new CardAction(ActionTypes.ImBack, "Voltar ao Menu", value: "Voltar ao menu")),
                card.GetThumbnailCard(
                    "Mouse",
                    "Problemas no funcionamento do Mouse",
                    "Não funciona, solicitar novo Mouse...",
                    new CardImage(url: "https://www.pichau.com.br/media/catalog/product/cache/1/small_image/215x162/9df78eab33525d08d6e5fb8d27136e95/3/_/3_13.jpg"),
                    new CardAction(ActionTypes.ImBack, "Opções", value: "Opções Mouse")),
                card.GetThumbnailCard(
                    "Teclado",
                    "Problemas no funcionamento do Teclado",
                    "Não funciona, solicitar novo teclado",
                    new CardImage(url: "https://www.pichau.com.br/media/catalog/product/cache/1/small_image/215x162/9df78eab33525d08d6e5fb8d27136e95/1/_/1_21_40.jpg"),
                    new CardAction(ActionTypes.ImBack, "Opções", value: "Opções Teclado")),
                card.GetThumbnailCard(
                    "Headset",
                    "Problemas com seu Headset",
                    "Não funciona, chiando, mudo...",
                    new CardImage(url: "http://www.aiheadsets.com/resize/Shared/images/Product/DC-200.jpg?bw=550&w=550&bh=550&h=550"),
                    new CardAction(ActionTypes.ImBack, "Opções", value: "Opções Headset")),
                card.GetThumbnailCard(
                    "Monitor",
                    "Problemas com seu Monitor",
                    "Não liga, tela preta, icones e letras estão grandes...",
                    new CardImage(url: "https://www.pichau.com.br/media/catalog/product/cache/1/small_image/215x162/9df78eab33525d08d6e5fb8d27136e95/l/e/led-2151_-_c_pia.jpg"),
                    new CardAction(ActionTypes.ImBack, "Opções", value: "Opções Monitor")),
                  card.GetThumbnailCard(
                    "Impressora",
                    "Problemas com sua Impressora",
                    "Folha travou, preciso de acesso, não liga...",
                    new CardImage(url: "https://pichau-media.s3.amazonaws.com/catalog/product/cache/1/small_image/215x162/9df78eab33525d08d6e5fb8d27136e95/m/1/m127fn-2.jpg"),
                    new CardAction(ActionTypes.ImBack, "Opções", value: "Opções Impressora")),
            };
        }

        public Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            throw new NotImplementedException();
        }

        public async Task MessageReceivedAsyncCardAction(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            var reply = context.MakeMessage();

            if (message.Text.ToLower().Contains("mouse"))
            {
                context.Call(opcoesMouseDialog.Build(), opcoesMouseDialog.ResumeAfter);
            }
            else if (message.Text.ToLower().Contains("teclado"))
            {
                PromptDialog.Choice(
                  context,
                  this.AfterChoiceSelected,
                  new[] { TecladoNaoFuncionaOption, SolicitarNovoTecladoOption, TecladoSumiuOption, VoltarMenuHardware },
                  "Selecione a categoria do Problema:",
                  "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                  attempts: 2);

                //await this.StartOverAsync(context, Resources.RootDialog_Welcome_Error);
            }
            else if (message.Text.ToLower().Contains("headset"))
            {
                PromptDialog.Choice(
                  context,
                  this.AfterChoiceSelected,
                  new[] { HeadsetNaoFuncionaOption, HeadSetMudoOption, VoltarMenuHardware },
                  "Selecione a categoria do Problema:",
                  "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                  attempts: 2);
                //await this.StartOverAsync(context, Resources.RootDialog_Welcome_Error);
            }
            else if (message.Text.ToLower().Contains("monitor"))
            {
                PromptDialog.Choice(
                  context,
                  this.AfterChoiceSelected,
                  new[] { MonitorNaoLigaOption, IconesGrandesOption, CoresDesconfiguradasOption, VoltarMenuHardware },
                  "Selecione a categoria do Problema:",
                  "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                  attempts: 2);
                //await this.StartOverAsync(context, Resources.RootDialog_Welcome_Error);
            }
            else if (message.Text.ToLower().Contains("menu"))
            {
                context.Done(true);

                //context.Call(new GreetingDialog(null, this), this.AfterDialogComplete);
                //context.Call(this.hardwareDialog, this.hardwareDialog.MessageResumeAfter);
            }
        }

        public async Task MessageResumeAfter(IDialogContext context, IAwaitable<DialogResponse> result)
        {
            context.Call(new PrincipalDialog(null, null, null, null), null);
        }

        public async Task AfterChoiceSelected(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                var selection = await result;
                var reply = context.MakeMessage();

                switch (selection)
                {
                    //Teclado
                    case TecladoNaoFuncionaOption:
                        await context.PostAsync("**Vamos lá, verifique os procedimentos abaixo:** \n\n **1.** Verifique se o Teclado está conectado à porta USB, você pode conectá-lo em outra porta. \n\n **2.** Tente reiniciar sua máquina.\n\n");
                        PromptDialog.Choice(
                         context,
                         this.AfterChoiceSelected,
                         new[] { OptionSim, OptionNao },
                         "**Os procedimentos acima resolveram o seu problema?**",
                         "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                         attempts: 2);
                        break;

                    case SolicitarNovoTecladoOption:
                        await context.PostAsync("Para esse processo é necessário abrir um chamado no [Gestão X](https://gestaox/Login.aspx)");
                        PromptDialog.Choice(
                         context,
                         this.AfterChoiceSelected,
                         new[] { PodeSimOption, NaoQueroOption },
                         "**Posso te ajudar com algum outro problema?**",
                         "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                         attempts: 2);
                        break;


                    case TecladoSumiuOption:
                        await context.PostAsync("Acione a CAF para ober as imagens durante o período que você não encontrou seu teclado. Após isso, abra um chamado no [Gestão X](https://gestaox/Login.aspx) \"**TI -> Hardware -> Teclado -> Solicitar teclado**\", anexando as imagens da CAF.");
                        PromptDialog.Choice(
                         context,
                         this.AfterChoiceSelected,
                         new[] { PodeSimOption, NaoQueroOption },
                         "**Posso te ajudar com algum outro problema?**",
                         "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                         attempts: 2);
                        //await context.PostAsync("Vamos lá, verifique se o Mouse está conectado à porta USB e se a luz vermelha do Mouse está acesa. \n\n Você também pode conectá-lo em outra porta. \n\n Ah, por favor, limpe a área de contato de seu Mouse. \n\n Esses procedimentos resolveram seu problema?");
                        //await this.StartAsync(context);
                        //context.Call(new RootLuisDialog(), this.AfterResetPassword);
                        break;


                    //Headset

                    case HeadsetNaoFuncionaOption:
                        PromptDialog.Choice(
                         context,
                         this.AfterChoiceSelected,
                         new[] { RamalFisicoOption, SoftPhoneOption },
                         "Qual aparelho você utiliza?",
                         "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                         attempts: 2);
                        //await context.PostAsync("Ok. Verifique se o Teclado está conectado à porta USB, você pode conectá-lo em outra porta. \n\n Caso não funcione, tente reiniciar sua máquina. \n\n Esses procedimentos resolveram seu problema?");
                        //await this.StartAsync(context);
                        //context.Call(new RootLuisDialog(), this.AfterResetPassword);
                        break;

                    case RamalFisicoOption:
                        await context.PostAsync("**Por favor, verifique os seguintes itens:**\n\n **1.** Headset está conectado corretamente. \n\n **2.** Tubo de voz pode estar entupido.");
                        PromptDialog.Choice(
                         context,
                         this.AfterChoiceSelected,
                         new[] { OptionSim, OptionNao },
                         "**Os procedimentos acima resolveram o seu problema?**",
                         "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                         attempts: 2);
                        break;

                    case SoftPhoneOption:
                        await context.PostAsync("**Por favor, Verifique os seguintes itens:**\n\n **1.** Tente realizar a troca da porta USB. \n\n **2.** Tubo de voz pode estar entupido.\n\n **3.** Mute está ativado. \n\n**4.** No computador a saida de som está habilitada.");
                        PromptDialog.Choice(
                         context,
                         this.AfterChoiceSelected,
                         new[] { OptionSim, OptionNao },
                         "**Os procedimentos acima resolveram o seu problema?**",
                         "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                         attempts: 2);
                        break;

                    case HeadSetMudoOption:
                        PromptDialog.Choice(
                         context,
                         this.AfterChoiceSelected,
                         new[] { RamalFisicoMudoOption, SoftPhoneMudoOption },
                         "Qual aparelho está mudo?",
                         "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                         attempts: 2);
                        break;

                    case RamalFisicoMudoOption:
                        await context.PostAsync("**Por favor, verifique os seguintes itens:**\n\n **1.** Headset está com LED aceso \n\n **2.** Tubo de voz pode estar entupido.");
                        PromptDialog.Choice(
                         context,
                         this.AfterChoiceSelected,
                         new[] { OptionSim, OptionNao },
                         "**Os procedimentos acima resolveram o seu problema?**",
                         "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                         attempts: 2);
                        break;

                    case SoftPhoneMudoOption:
                        await context.PostAsync("**Por favor, Verifique os seguintes itens:**\n\n **1.** Mute está ativado. \n\n **2.** Tubo de voz pode estar entupido.\n\n **3.** No computador a saida de som está habilitada ");
                        PromptDialog.Choice(
                         context,
                         this.AfterChoiceSelected,
                         new[] { OptionSim, OptionNao },
                         "**Os procedimentos acima resolveram o seu problema?**",
                         "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                         attempts: 2);
                        break;

                    //Monitor
                    case MonitorNaoLigaOption:
                        await context.PostAsync("**Por favor, verifique os seguintes itens:** \n\n**1.** Verifique o cabo de força conectado atrás do monitor e na tomada.\n\n**2.** Verifique se led do monitor que fica no botão power ou na barra inferior acendeu.\n\n **3.** Aperte o botão que liga o monitor.");
                        PromptDialog.Choice(
                         context,
                         this.AfterChoiceSelected,
                         new[] { OptionSim, OptionNao },
                         "**Os procedimentos acima resolveram o seu problema?**",
                         "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                         attempts: 2);
                        break;

                    case IconesGrandesOption:
                        await context.PostAsync("**Por favor, verifique a resolução de tela.**\n\nClique com o botão direito do mouse na area de trabalho, vá em \"**Configurações de exibição -> Tela -> Resolução.**\"");
                        PromptDialog.Choice(
                         context,
                         this.AfterChoiceSelected,
                         new[] { OptionSim, OptionNao },
                         "**O procedimento acima resolveu o seu problema?**",
                         "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                         attempts: 2);
                        //await context.PostAsync("- Verifique se o Headset está conectado ao Telefone e se o cabo não está danificado. \n\n - Conecte o cabo até escutar o clique. Isso resolveu seu problema? ");
                        //await this.StartAsync(context);
                        break;

                    case CoresDesconfiguradasOption:
                        await context.PostAsync("Neste caso peço que aperte o cabo de vídeo atrás do monitor e também na conexão com seu computador.");
                        PromptDialog.Choice(
                         context,
                         this.AfterChoiceSelected,
                         new[] { OptionSim, OptionNao },
                         "**O procedimento acima resolveu o seu problema?**",
                         "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                         attempts: 2);
                        //await context.PostAsync("Ok. Verifique se o tubo de voz está devidamente limpo e tente novamente. Isso resolveu seu problema? ");
                        //await this.StartAsync(context);
                        break;

                    //Impressora
                    case NaoSaiuOption:
                        await context.PostAsync("**Neste caso preciso que você faça a liberação da impressão no papercut.** \n\n Acesse o [Link](http://print_orion:9191/user) \n\n**1.** Efetue login e libere a impressão. \n\n**2.** Verifique se tem papel na gaveta.");
                        PromptDialog.Choice(
                         context,
                         this.AfterChoiceSelected,
                         new[] { OptionSim, OptionNao },
                         "**Os procedimentos acima resolveram seu problema?**",
                         "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                         attempts: 2);
                        //await context.PostAsync("Ok. Verifique se o Teclado está conectado à porta USB, você pode conectá-lo em outra porta. \n\n Caso não funcione, tente reiniciar sua máquina. \n\n Esses procedimentos resolveram seu problema?");
                        //await this.StartAsync(context);
                        //context.Call(new RootLuisDialog(), this.AfterResetPassword);
                        break;

                    case ImpressoraTravouOption:
                        await context.PostAsync("**Por favor, verifique o seguinte item:** \n\n **1.** Verifique se o cabo de rede esta devidamente conectado. \n\n**2.** Verifique se tem papel na gaveta. \n\n**3.** Verifique se aparece alguma mensagem no display.");
                        PromptDialog.Choice(
                         context,
                         this.AfterChoiceSelected,
                         new[] { OptionSim, OptionNao },
                         "**Os procedimentos acima resolveram seu problema?**",
                         "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                         attempts: 2);
                        //await context.PostAsync("- Verifique se o Headset está conectado ao Telefone e se o cabo não está danificado. \n\n - Conecte o cabo até escutar o clique. Isso resolveu seu problema? ");
                        //await this.StartAsync(context);
                        break;

                    case MovimentarImpressoraOption:
                        await context.PostAsync("Neste caso preciso que você abra um chamado no [Gestão X](https://gestaox/Login.aspx) no fluxo, \"**TI / Hardware / Desktop / Movimentação**\"");
                        PromptDialog.Choice(
                         context,
                         this.AfterChoiceSelected,
                         new[] { PodeSimOption, NaoQueroOption },
                         "**Posso te ajudar com algum outro problema?**",
                         "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                         attempts: 2);
                        //await context.PostAsync("Ok. Verifique se o tubo de voz está devidamente limpo e tente novamente. Isso resolveu seu problema? ");
                        //await this.StartAsync(context);
                        break;

                    case FolhaPresaOption:
                        await context.PostAsync("**Acione a equipe de suporte para verificar.**\n\nRegistre um chamado no [Gestão X](https://gestaox/Login.aspx)");
                        PromptDialog.Choice(
                         context,
                         this.AfterChoiceSelected,
                         new[] { PodeSimOption, NaoQueroOption },
                         "**Posso te ajudar com algum outro problema?** ",
                         "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                         attempts: 2);
                        //await context.PostAsync("Ok. Verifique se o tubo de voz está devidamente limpo e tente novamente. Isso resolveu seu problema? ");
                        //await this.StartAsync(context);
                        break;

                    case AcessoImpressoraOption:
                        await context.PostAsync("**Preciso que você registre um chamado solicitando acesso.**\n\nRegistre um chamado no [Gestão X](https://gestaox/Login.aspx) no fluxo,  **\"TI  / Usuários / acesso / Liberação de acesso**\"");
                        PromptDialog.Choice(
                         context,
                         this.AfterChoiceSelected,
                         new[] { PodeSimOption, NaoQueroOption },
                         "**Posso te ajudar com algum outro problema?**",
                         "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                         attempts: 2);
                        //await context.PostAsync("Ok. Verifique se o tubo de voz está devidamente limpo e tente novamente. Isso resolveu seu problema? ");
                        //await this.StartAsync(context);
                        break;

                    case ImpressoraNaoFuncionaOption:
                        await context.PostAsync("**Neste caso preciso que você verifique se o cabo de força esta conectado.** \n\nCaso o problema persista, teste um outro aparelho na tomada.");
                        PromptDialog.Choice(
                         context,
                         this.AfterChoiceSelected,
                         new[] { OptionSim, OptionNao },
                         "**Os procedimentos acima resolveram seu problema?**",
                         "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                         attempts: 2);
                        //await context.PostAsync("Ok. Verifique se o tubo de voz está devidamente limpo e tente novamente. Isso resolveu seu problema? ");
                        //await this.StartAsync(context);
                        break;


                    //Demais Solicitações

                    case OptionSim:
                        await context.PostAsync("Ah, que bom que pude lhe ajudar! \n\n **#EstamosJuntos** \n\n **#NossoCoraçãoÉDigital**💙");
                        PromptDialog.Choice(
                         context,
                         this.AfterChoiceSelected,
                         new[] { PodeSimOption, NaoQueroOption },
                         "**Posso te ajudar com algum outro problema?** ",
                         "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                         attempts: 2);
                        break;

                    case OptionNao:
                        await context.PostAsync("Ah, que pena que não consegui te ajudar!😢 \n\n Neste caso peço que abra um chamado no [Gestão X](http://gestaox/Login.Aspx)");
                        PromptDialog.Choice(
                        context,
                        this.AfterChoiceSelected,
                        new[] { PodeSimOption, NaoQueroOption },
                        "**Posso te ajudar com algum outro problema?** ",
                        "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
                        attempts: 2);
                        break;

                    case PodeSimOption:
                        await this.BackToMenuAsync(context);
                        break;

                    case NaoQueroOption:
                        await context.PostAsync("Ok, caso precise conversar comigo novamente é só chamar! \n\n Um abraço e até a próxima!");
                        //context.Call(new GreetingDialog(null, this), this.AfterDialogComplete);

                        //await this.StartAsync(context);
                        break;
                    //case ResetPasswordOption:
                    //    context.Call(new ResetPasswordDialog(), this.AfterResetPassword);
                    //    break;

                    case VoltarMenuHardware:
                        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                        reply.Attachments = GetCardsAttachments();
                        await context.PostAsync(reply);
                        context.Wait(this.OnOptionSelected);
                        break;

                        //case VoltarMenuSoftware:
                        //    reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                        //    reply.Attachments = GetCardsSoftwareAttachments();
                        //    await context.PostAsync(reply);
                        //    context.Wait(this.OnOptionSelected);
                        //    break;
                }
            }
            catch (TooManyAttemptsException)
            {
                await this.StartAsync(context);
            }
        }

        public virtual async Task BackToMenuAsync(IDialogContext context)
        {
            var reply = context.MakeMessage();
            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            reply.Attachments = GetCardsAttachments();

            await context.PostAsync(reply);

            context.Wait(this.OnOptionSelected);
        }

        private async Task OnOptionSelected(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            //var message = await result;
            //var reply = context.MakeMessage();

            //if (message.Text.ToLower().Contains("hardware"))
            //{
            //    reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            //    reply.Attachments = GetCardsHardwareAttachments();
            //    await context.PostAsync(reply);
            //}
        }

        private async Task AfterDialogComplete(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            await context.PostAsync("Vi que você interagiu comigo sobre um problema de Hardware... Para adiantar vou exibir o menu da nossa última conversa, ok?");
            await this.StartAsync(context);
        }
    }
}