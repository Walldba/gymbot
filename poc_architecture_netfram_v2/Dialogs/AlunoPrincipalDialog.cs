﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Connector;
using poc_architecture_netfram_v2.Dialogs.Interfaces;
using poc_architecture_netfram_v2.Models;
using poc_architecture_netfram_v2.Models.Cards;
using poc_architecture_netfram_v2.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace poc_architecture_netfram_v2.Dialogs
{
    [Serializable]
    public class AlunoPrincipalDialog : IBaseDialogCard
    {

        private readonly AulasDialog aulasDialog;
        private readonly DicasDialog dicasDialog;
        private readonly FichaDialog fichaDialog;
        //private readonly SobreAutorDialog sobreAutorDialog;
        //private readonly CursosDialog cursosDialog;
        //private readonly CertificacoesCurriculoDialog certificacoesCurriculoDialog;
        //private readonly RootLUISDialog rootLuisDialog;

        //public PrincipalDialog(HardwareCardsDialog hardwareDialog)
        public AlunoPrincipalDialog(AulasDialog aulasDialog, DicasDialog dicasDialog, FichaDialog fichaDialog)
        {
            //this.hardwareDialog = hardwareDialog;
            //this.sobreAutorDialog = sobreAutorDialog;
            //this.cursosDialog = cursosDialog;
            this.aulasDialog = aulasDialog;
            this.dicasDialog = dicasDialog;
            this.fichaDialog = fichaDialog;
            //this.rootLuisDialog = rootLuisDialog;
        }

        /// <summary>
        /// Método inicial, chamado assim que o objeto é criado
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task StartAsync(IDialogContext context)
        {
            ClientesAPIServices _clientes = new ClientesAPIServices();
            List<Clientes> listaClientes = new List<Clientes>();

            listaClientes = _clientes.ListarModalidade();

            foreach (var item in listaClientes)
            {
                await context.PostAsync($"Então {item.Nome}, o que você deseja hoje?");
            }

            Thread.Sleep(1000);

          
            var reply = context.MakeMessage();
            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            reply.Attachments = GetCardsAttachments();
            

            await context.PostAsync(reply);

            context.Wait(this.MessageReceivedAsyncCardAction);
        }


        public async Task MessageReceivedAsyncCardAction(IDialogContext context, IAwaitable<IMessageActivity> result)
        {

            var message = await result;
            var reply = context.MakeMessage();

            if (message.Text.ToLower().Contains("aulas"))
            {
                context.Call(this.aulasDialog, this.MessageResumeAfter);
            }
            else if (message.Text.ToLower().Contains("dicas"))
            {
                context.Call(this.dicasDialog, this.MessageResumeAfter);
            }
            else if (message.Text.ToLower().Contains("ficha"))
            {
                context.Call(this.fichaDialog, this.MessageResumeAfter);
            }
        }

        public async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            //Thread.Sleep(1000);            
            //context.Wait(this.OnOptionSelected);
            throw new NotImplementedException();
        }

        private Task AfterDialogComplete(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            throw new NotImplementedException();
        }

        private static IList<Attachment> GetCardsAttachments()
        {
            CardModel card = new CardModel();

            return new List<Attachment>()
            {
                card.GetHeroCard(
                    "Aulas de Hoje",
                    "",
                    "Saiba os horários das aulas de hoje",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu-GymBot/Menu_Aulas.png"),
                    new CardAction(ActionTypes.ImBack, "Aulas de hoje ⏰", value: "Aulas de hoje")),
                card.GetHeroCard(
                    "Dicas da Duda",
                    "",
                    "Dicas nutricionais para otimizar seus treinos",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu-GymBot/Menu_Dicas.png"),
                    new CardAction(ActionTypes.ImBack, "Ver dicas🍉", value: "Ver dicas")),
                card.GetHeroCard(
                    "Ficha de exercícios",
                    "",
                    "Veja seu plano de treinamento",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu-GymBot/Menu_Frequencia.png"),
                    new CardAction(ActionTypes.ImBack, "Ficha de exercícios 📋", value: "Ficha de exercícios")),

            };
        }

        public async Task MessageResumeAfter(IDialogContext context, IAwaitable<DialogResponse> result)
        {
            var reply = context.MakeMessage();
            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            reply.Attachments = GetCardsAttachments();


            await context.PostAsync(reply);

            context.Wait(this.MessageReceivedAsyncCardAction);
        }

        IList<Attachment> IBaseDialogCard.GetCardsAttachments()
        {
            throw new NotImplementedException();
        }


    }
}