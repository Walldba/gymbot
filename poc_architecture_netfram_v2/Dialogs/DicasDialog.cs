﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using poc_architecture_netfram_v2.Dialogs.Interfaces;
using poc_architecture_netfram_v2.Models;
using poc_architecture_netfram_v2.Models.Cards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace poc_architecture_netfram_v2
{
    [Serializable]
    public class DicasDialog : IBaseDialogCard
    {
        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("Oia a dica aew");
            var reply = context.MakeMessage();

            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            reply.Attachments = GetCardsAttachments();
            await context.PostAsync(reply);

            context.Wait(this.MessageReceivedAsyncCardAction);


        }

        public IList<Attachment> GetCardsAttachments()
        {
            CardModel card = new CardModel();

            return new List<Attachment>()
            {
                card.GetHeroCard(
                    "Aposte na salada pós-treino",
                    "Saladas podem ser uma ótima opção de alimentação pós-treino. As ricas em proteínas, como a salada de atum, ajudam a recuperar os músculos e combatem o colesterol ruim.",
                    "",
                    new CardImage(url: "https://abrilboaforma.files.wordpress.com/2017/03/saladanopote.jpg"),
                    new CardAction(ActionTypes.OpenUrl, "Saiba mais", value: "https://boaforma.abril.com.br/dieta/11-dicas-de-alimentacao-para-acelerar-os-resultados-da-academia/")),
                card.GetHeroCard(
                    "Aproveite as frutas como suas aliadas",
                    "Ótimas como sobremesa, as frutas podem ser consumidas tanto puras quanto em receitas. Tente variá-las por dia da semana ou seguir ideias rapidinhas.",
                    "",
                    new CardImage(url: "https://static.vix.com/pt/sites/default/files/styles/large/public/f/frutas-variedades-dieta-0916-1400x800.jpg?itok=VCZyTVTv"),
                    new CardAction(ActionTypes.OpenUrl, "Saiba mais", value: "https://boaforma.abril.com.br/dieta/11-dicas-de-alimentacao-para-acelerar-os-resultados-da-academia/")),
                card.GetHeroCard(
                    "Voltar ao menu",
                    "Clique abaixo para voltar ao Menu",
                    "Clique abaixo para voltar ao Menu",
                    new CardImage(url: ""),
                    new CardAction(ActionTypes.ImBack, "Voltar ao menu", value: "Voltar ao menu")),

            };
        }

        public Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            throw new NotImplementedException();
        }

        public async Task MessageReceivedAsyncCardAction(IDialogContext context, IAwaitable<IMessageActivity> result)
        {

            var message = await result;
            var reply = context.MakeMessage();

            if (message.Text.ToLower().Contains("menu"))
            {
                context.Done(true);
            }
        }

        public Task MessageResumeAfter(IDialogContext context, IAwaitable<DialogResponse> result)
        {
            throw new NotImplementedException();
        }


    }
}