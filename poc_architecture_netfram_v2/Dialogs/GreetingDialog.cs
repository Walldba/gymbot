﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using poc_architecture_netfram_v2.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace poc_architecture_netfram_v2.Dialogs
{
    [Serializable]
    public class GreetingDialog : IDialog<IMessageActivity>
    {
        public const string SouAluno = "Sou aluno 😎";
        public const string NaoSouAluno = "Não sou aluno 😀";

        private readonly AlunoPrincipalDialog alunoPrincipalDialog;
        private readonly NaoAlunoPrincipalDialog naoAlunoPrincipalDialog;
        //private readonly SobreAutorDialog sobreAutorDialog;

        public GreetingDialog()
        {
            
        }

        public GreetingDialog(AlunoPrincipalDialog alunoPrincipalDialog, NaoAlunoPrincipalDialog naoAlunoPrincipalDialog)
        {
            this.alunoPrincipalDialog = alunoPrincipalDialog;
            this.naoAlunoPrincipalDialog = naoAlunoPrincipalDialog;
        }



        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);
        }



        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> messageActivity)
        {

            var activity = await messageActivity as Activity;

            var menssagem = activity.CreateReply();

            var heroCard = new HeroCard();
            heroCard.Title = "Gym Bot ☝";
            heroCard.Images = new List<CardImage>
                {
                    new CardImage ("https://raw.githubusercontent.com/walldba/JL-Project/master/Menu_GymBot/icon.png")
                };

            menssagem.Attachments.Add(heroCard.ToAttachment());


            await context.PostAsync(menssagem);
            Thread.Sleep(1000);
            await context.PostAsync("Eu sou o GymBot, o assistente virtual da Academia Fictícia!");
            Thread.Sleep(1000);
            await context.PostAsync("Irei trazer informações sobre seu treino e frequência caso seja aluno da nossa academia, caso não seja, irei trazer informações sobre a academia.💪");

            Thread.Sleep(1000);
            PromptDialog.Choice(
            context,
            this.AfterChoiceSelected,
            new[] { SouAluno, NaoSouAluno },
            "Você já é aluno?",
            "Me desculpe, mas eu preciso que você responda se eu posso ou não te ajudar com algum outro problema.:",
            attempts: 2);


        }

        public async Task AfterChoiceSelected(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                var selection = await result;
                switch (selection)
                {
                    case SouAluno:
                        //Solicitar CPF do usuário e validar na base ficticia 
                        context.Call(this.alunoPrincipalDialog, this.MessageResumeAfter);
                        break;

                    case NaoSouAluno:
                        await context.PostAsync("Neste caso irei trazer informações sobre a nossa academia 😉");
                        context.Call(this.naoAlunoPrincipalDialog, this.MessageResumeAfter);
                        break;

                }
            }
            catch (TooManyAttemptsException)
            {
                await this.StartAsync(context);
            }

        }
        public async Task MessageResumeAfter(IDialogContext context, IAwaitable<DialogResponse> result)
        {
            context.Done(true);
        }

        //private static void SetUserName(IDialogContext context, string value)
        //{
        //    context.UserData.SetValue<string>(key_client_user_name, value);
        //}

        //private static string GetUserName(IDialogContext context)
        //{
        //    var userName = String.Empty;
        //    context.UserData.TryGetValue<string>(key_client_user_name, out userName);
        //    return userName;
        //}
    }
}