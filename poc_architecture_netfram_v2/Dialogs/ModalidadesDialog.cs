﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using poc_architecture_netfram_v2.Dialogs.Interfaces;
using poc_architecture_netfram_v2.Models;
using poc_architecture_netfram_v2.Models.Cards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace poc_architecture_netfram_v2.Dialogs
{
    [Serializable]
    public class ModalidadesDialog : IBaseDialogCard
    {

        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("Aqui vc pode ver as nossas modalidades. 😎🤙");
            Thread.Sleep(1000);

            var reply = context.MakeMessage();
            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            reply.Attachments = GetCardsAttachments();

            await context.PostAsync(reply);

            context.Wait(this.MessageReceivedAsyncCardAction);
        }

        public IList<Attachment> GetCardsAttachments()
        {
            CardModel card = new CardModel();

            return new List<Attachment>()
            {

                card.GetThumbnailCard2(
                    "Circuito Funcional",
                    "Essa é mais outra modalidade de exercícios físicos que foge do padrão repetitivo de treinos da academia. Não foi por acaso que esse tipo de atividade física ficou tão famosa e popular. Aula perfeita para quem quer emagrecer.",
                    "Seg - Qua - Sex\n\n 18 às 19 - Professor Gerson",
                    new CardImage(url: "http://www.fitwayjaragua.com.br/wp-content/uploads/2017/09/CIRCUITO-FUNCIONAL.jpg")),
                    //new CardAction(ActionTypes.ImBack, "Funcional", value: "Funcional")),
                card.GetThumbnailCard2(
                    "Muay Thai",
                    "É conhecida mundialmente como A Arte das Oito Armas, pois se caracteriza pelo uso combinado dos dois punhos + dois cotovelos + dois joelhos + dois canelas e pés, e associado a uma forte preparação física.",
                    "Terça e Quinta \n\n 17 às 19 - Professor Daniel",
                    new CardImage(url: "http://www.sitedebelezaemoda.com.br/wp-content/uploads/2014/09/aulas-que-mais-emagrecem13.jpg")),
                    //new CardAction(ActionTypes.OpenUrl, "Saiba mais", value: "https://www.microsoft.com/pt-br/learning/mcts-certification.aspx")),
                card.GetThumbnailCard2(
                    "Musculação",
                    "O treinamento de força, conhecido popularmente como musculação é uma forma de exercício contra resistência, praticado normalmente em ginásios, para o treinamento e desenvolvimento dos músculos esqueléticos.",
                    " Segunda a Domingo \n\n Horário livre - Instrutores Gustavo e Thiago",
                    new CardImage(url: "https://www.vestiremaquiar.com.br/wp-content/uploads/2016/01/Como-emagrecer-fazendo-musculac%CC%A7a%CC%83o-02.jpg")),
                    //new CardAction(ActionTypes.OpenUrl, "Saiba mais", value: "https://www.exin.com/br/pt/qualification-program/exin-it-service-management-based-isoiec-20000?language_content_entity=pt-br")),
                card.GetThumbnailCard2(
                    "Pilates",
                    "Pilates é um conjunto de exercícios criados pelo alemão chamado Joseph Pilates, em meados de 1920, que são realizados nos Solo ou em Equipamentos exclusivos, que visa o total e completo controle e conexão.",
                    "Quarta e Sexta \n\n 09 às 11 - Professor Bernardo",
                    new CardImage(url: "http://ortocenter.com.br/wp-content/uploads/2017/05/499314418-1040x555.jpg")),
                    //new CardAction(ActionTypes.OpenUrl, "Saiba mais", value: "https://www.exin.com/br/pt/certifications/information-security-foundation-based-iso-iec-27002-exam?language_content_entity=pt-br")),
                card.GetThumbnailCard(
                    "Voltar ao Menu",
                    "Não estava aqui o que procurava? Volte ao Menu :D",
                    "",
                    new CardImage(url: "http://sustainablearchaeology.org/img/backtomenu.png"),
                    new CardAction(ActionTypes.ImBack, "Voltar ao Menu", value: "Voltar ao menu")),
            };
        }

        public Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            throw new NotImplementedException();
        }

        public async Task MessageReceivedAsyncCardAction(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            var reply = context.MakeMessage();

            if (message.Text.ToLower().Contains("menu"))
            {
                context.Done(true);
            }
        }

        public Task MessageResumeAfter(IDialogContext context, IAwaitable<DialogResponse> result)
        {
            throw new NotImplementedException();
        }


    }
}