﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Connector;
using poc_architecture_netfram_v2.Dialogs.Formulario;
using poc_architecture_netfram_v2.Dialogs.Interfaces;
using poc_architecture_netfram_v2.Models;
using poc_architecture_netfram_v2.Models.Cards;
using poc_architecture_netfram_v2.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace poc_architecture_netfram_v2.Dialogs
{
    [Serializable]
    public class NaoAlunoPrincipalDialog : IBaseDialogCard
    {

        //private readonly HardwareCardsDialog hardwareDialog;
        private readonly SobreDialog sobreDialog;
        private readonly ModalidadesDialog modalidadesDialog;
        private readonly Pedido pedido;
        private readonly RootLUISDialog rootLUISDialog;
        //private readonly CertificacoesCurriculoDialog certificacoesCurriculoDialog;
        //private readonly RootLUISDialog rootLuisDialog;

        //public PrincipalDialog(HardwareCardsDialog hardwareDialog)
        public NaoAlunoPrincipalDialog(SobreDialog sobreDialog, ModalidadesDialog modalidadesDialog,Pedido pedido, RootLUISDialog rootLUISDialog)
        {
            //this.hardwareDialog = hardwareDialog;
            this.sobreDialog = sobreDialog;
            this.modalidadesDialog = modalidadesDialog;
            this.pedido = pedido;
            this.rootLUISDialog = rootLUISDialog;
            //this.certificacoesCurriculoDialog = certificacoesCurriculoDialog;
            //this.rootLuisDialog = rootLuisDialog;
        }

        /// <summary>
        /// Método inicial, chamado assim que o objeto é criado
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task StartAsync(IDialogContext context)
        {
            var reply = context.MakeMessage();
            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            reply.Attachments = GetCardsAttachments();


            await context.PostAsync(reply);

            context.Wait(this.MessageReceivedAsyncCardAction);
        }


        public async Task MessageReceivedAsyncCardAction(IDialogContext context, IAwaitable<IMessageActivity> result)
        {

            var message = await result;
            var reply = context.MakeMessage();

            if (message.Text.ToLower().Contains("sobre"))
            {
                context.Call(this.sobreDialog, this.MessageResumeAfter);
            }
            else if (message.Text.ToLower().Contains("modalidades"))
            {
                context.Call(this.modalidadesDialog, this.MessageResumeAfter);
            }
            else if (message.Text.ToLower().Contains("comigo"))
            {
                context.Call(this.modalidadesDialog, this.MessageResumeAfter);
            }
            else if (message.Text.ToLower().Contains("agendar"))
            {
                context.Call(this.pedido, this.MessageResumeAfter);
            }
            else if (message.Text.ToLower().Contains("conversar"))
            {
                await context.PostAsync("Então, aqui nós podemos bater um papo bacana. \n\n Mas lembre-se que eu ainda estou aprendendo a responder várias perguntas.");
                context.Call(this.rootLUISDialog, this.rootLUISDialog.AfterLuisDialog);
            }
            else if (message.Text == "Voltar ao menu")
            {
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = GetCardsAttachments();
                await context.PostAsync(reply);
                //await this.StartOverAsync(context, Resources.RootDialog_Support_Message);
            }
        }

        public async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            //Thread.Sleep(1000);            
            //context.Wait(this.OnOptionSelected);
            throw new NotImplementedException();
        }

        private Task AfterDialogComplete(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            throw new NotImplementedException();
        }

        private static IList<Attachment> GetCardsAttachments()
        {
            CardModel card = new CardModel();

            return new List<Attachment>()
            {
                card.GetHeroCard(
                    "Nossa História",
                    "",
                    "Saiba sobre nossa academia.",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu-GymBot/Menu_Sobre.png"),
                    new CardAction(ActionTypes.ImBack, "Sobre", value: "Sobre")),
                card.GetHeroCard(
                    "Modalidades",
                    "",
                    "Saiba informações sobre as modalidades disponíveis.",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu-GymBot/Menu_Modalidades.png"),
                    new CardAction(ActionTypes.ImBack, "Modalidades", value: "Modalidades")),
                card.GetHeroCard(
                    "Agende sua avaliação",
                    "",
                    "Agende sua avaliação que entraremos em contato.",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu-GymBot/Menu_Agendar.png"),
                    new CardAction(ActionTypes.ImBack, "Agendar", value: "Agendar")),
                card.GetHeroCard(
                    "Converse com o Gym Bot",
                    "",
                    "Tire dúvidas e troque uma boa conversa com o Gym.",
                    new CardImage(url: "https://raw.githubusercontent.com/walldba/JL-Project/master/Menu-GymBot/Menu_Luis.png"),
                    new CardAction(ActionTypes.ImBack, "Conversar", value: "Conversar")),

            };
        }

        public async Task MessageResumeAfter(IDialogContext context, IAwaitable<DialogResponse> result)
        {
            var reply = context.MakeMessage();
            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            reply.Attachments = GetCardsAttachments();


            await context.PostAsync(reply);

            context.Wait(this.MessageReceivedAsyncCardAction);
            //context.Call(new PrincipalDialog(new SobreAutorDialog(), new CursosDialog(), new CertificacoesCurriculoDialog(), new RootLUISDialog()), MessageResumeAfter);
            //context.Call(this, MessageResumeAfter);

            //context.Done(this.sobreAutorDialog);
            //throw new NotImplementedException();
        }

        IList<Attachment> IBaseDialogCard.GetCardsAttachments()
        {
            throw new NotImplementedException();
        }


    }
}