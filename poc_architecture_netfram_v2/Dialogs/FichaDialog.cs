﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using poc_architecture_netfram_v2.Dialogs.Interfaces;
using poc_architecture_netfram_v2.Models;
using poc_architecture_netfram_v2.Models.Cards;
using poc_architecture_netfram_v2.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace poc_architecture_netfram_v2.Dialogs
{
    [Serializable]
    public class FichaDialog : IBaseDialogCard
    {
        public async Task StartAsync(IDialogContext context)
        {
            var reply = context.MakeMessage();
            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            reply.Attachments = GetCardsAttachments();

            await context.PostAsync(reply);

            context.Wait(this.MessageReceivedAsyncCardAction);
        }

        public IList<Attachment> GetCardsAttachments()
        {
            List<Attachment> at = new List<Attachment>();

            FichasAPIServices _fichas = new FichasAPIServices();
            List<Fichas> listaFichas = new List<Fichas>();

            listaFichas = _fichas.ListarExercicios();

            foreach (var item in listaFichas)
            {
                CardModel CardTub = new CardModel();
                var itemAT = new List<Attachment>()
                    {
                        CardTub.GetThumbnailCard(
                        item.NomeExercicio,
                        item.QtdRepeticoes,
                        item.Peso,
                        new CardImage(url: $"{item.UrlImagem}"),
                        new CardAction(ActionTypes.ShowImage, "Ver Imagem", value: $"{item.ValorAcao}")),
                    };

                at.Add(itemAT.FirstOrDefault());
            }
            CardModel card = new CardModel();

            List<Attachment> attach = new List<Attachment>()
            {
                card.GetThumbnailCard(
                    "Voltar ao Menu",
                    "",
                    "Clique abaixo para voltar ao Menu",
                    new CardImage(url: "http://sustainablearchaeology.org/img/backtomenu.png"),
                    new CardAction(ActionTypes.ImBack, "Voltar ao Menu", value: "Voltar ao menu"))
            };

            at.Add(attach.First());

            return at;
        }

        public Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            throw new NotImplementedException();
        }

        public async Task MessageReceivedAsyncCardAction(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            var reply = context.MakeMessage();

            if (message.Text.ToLower().Contains("menu"))
            {
                context.Done(true);
            }
        }

        public Task MessageResumeAfter(IDialogContext context, IAwaitable<DialogResponse> result)
        {
            throw new NotImplementedException();
        }


    }
}