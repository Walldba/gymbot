﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace poc_architecture_netfram_v2.Dialogs
{
    [LuisModel("7004a6ba-8e89-4a3e-b3f2-5bfb5f79c51f", "d1e30db4bbf64679b6374a4febe5ec14")]
    [Serializable]

    public class RootLUISDialog : LuisDialog<bool>
    {
        //Opções
        public const string OptionSim = "Sim";
        //public const string OptionNao = "Não";
        public const string PodeSimOption = "Pode sim =D";
        public const string NaoQueroOption = "Não. Quero sair do diálogo.";

        

        [LuisIntent("None")]
        public async Task NoneAsync(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Desculpe, mas não entendi o que você quis dizer.");
            await PromptChoiceAsync(context, result, "**Deseja fazer outra pergunta?**");
            
            //await context.PostAsync("Desculpe. Você pode reformular a sua dúvida?");
            //context.Wait(MessageReceived);
        }

        [LuisIntent("SaberSobreProfessores")]
        public async Task SaberSobreProfessoresAsync(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Todos nossos professores são formados e qualificados para oferecer o melhor acompanhamento.");
            await PromptChoiceAsync(context, result, "**Deseja fazer outra pergunta?**");

            //await context.PostAsync("Desculpe. Você pode reformular a sua dúvida?");
            //context.Wait(MessageReceived);
        }

        [LuisIntent("SaberHorarioFuncionamento")]
        public async Task SaberHorarioFuncionamentoAsync(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Nossas portas estão abertas 24h! É só chegar e treinar pesado 💪");
            await PromptChoiceAsync(context, result, "**Deseja fazer outra pergunta?**");

            //await context.PostAsync("Desculpe. Você pode reformular a sua dúvida?");
            //context.Wait(MessageReceived);
        }

        public async Task PromptChoiceAsync(IDialogContext context, LuisResult result, string texto)
        {

            PromptDialog.Choice(
            context,
            this.AfterChoiceSelected,
            new[] { OptionSim, NaoQueroOption },
            texto,
            "Me desculpe, mas eu preciso que você responda se eu posso ou não te ajudar com algum outro problema.:",
            attempts: 2);
        }

        public async Task AfterChoiceSelected(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                var selection = await result;
                switch (selection)
                {
                    case OptionSim:
                        await context.PostAsync("Ah, que legal! Digita sua pergunta então :) \n\n");

                        break;

                    case NaoQueroOption:
                        await context.PostAsync("Adorei conversar com você! Se quiser conversar comigo novamente é só chamar 😎");
                        context.Call(new GreetingDialog(), null);
                        //await this.StartAsync(context);
                        break;                   
                }
            }
            catch (TooManyAttemptsException)
            {
                await this.StartAsync(context);
            }

        }

        public async Task AfterLuisDialog(IDialogContext context, IAwaitable<bool> result)
        {
            context.Done(true);
        }
    }
}