﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using poc_architecture_netfram_v2.Dialogs;
using poc_architecture_netfram_v2.Dialogs.Interfaces;
using poc_architecture_netfram_v2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace poc_architecture_netfram_v2
{
    [Serializable]
    public class SobreDialog : IBaseDialogCard
    {
        public const string PodeSimOption = "Pode sim =D";
        public const string NaoQueroOption = "Não. Quero sair do diálogo.";

        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("Desde 2010, somos orientados por um único pensamento: oferecer tudo o que você precisa para estar bem.");
            Thread.Sleep(1000);
            await context.PostAsync("Quem procura uma melhor qualidade de vida através de uma rotina de exercícios, alto padrão de atendimento, excelente localização e custo acessível, encontra na Fórmula a solução mais fácil e inteligente.");
            Thread.Sleep(1000);
            await context.PostAsync("É por isso que a Academia Fictícia é na medida certa para você.");
            Thread.Sleep(1000);

            PromptDialog.Choice(
             context,
             this.AfterChoiceSelected,
             new[] { PodeSimOption, NaoQueroOption },
             "**Posso te ajudar com algum outro problema?**",
             "Me desculpe. Não entendi isso. Preciso que você selecione uma das opções abaixo:",
             attempts: 2);
        }

        public async Task AfterChoiceSelected(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                var selection = await result;
                switch (selection)
                { 

                    case PodeSimOption:
                        context.Done(false);
                        break;

                    case NaoQueroOption:
                        await context.PostAsync("Foi ótimo conversar com você 😎✌. Estaremos aguardando sua visita!");
                        context.Call(new GreetingDialog(), null);

                        //await this.StartAsync(context);
                        break;
                }
            }
            catch (TooManyAttemptsException)
            {
                await this.StartAsync(context);
            }

        }

        public IList<Attachment> GetCardsAttachments()
        {
            throw new NotImplementedException();
        }

        public Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            throw new NotImplementedException();
        }

        public Task MessageReceivedAsyncCardAction(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            throw new NotImplementedException();
        }

        public Task MessageResumeAfter(IDialogContext context, IAwaitable<DialogResponse> result)
        {
            throw new NotImplementedException();
        }


    }
}