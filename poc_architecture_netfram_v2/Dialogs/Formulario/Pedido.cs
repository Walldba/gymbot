﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Connector;
using poc_architecture_netfram_v2.Dialogs.Interfaces;
using poc_architecture_netfram_v2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace poc_architecture_netfram_v2.Dialogs.Formulario
{
    [Serializable]
    public class Pedido : IBaseDialogCard
    {
        public Modalidades Modalidades { get; set; }
        public Turno Turno { get; set; }
        public Professor Professor { get; set; }

        public string Nome { get; set; }

        public string Telefone { get; set; }

        public string Email { get; set; }

        public static IForm<Pedido> BuildForm()
        {
            var form = new FormBuilder<Pedido>();
            form.Configuration.DefaultPrompt.ChoiceStyle = ChoiceStyleOptions.Buttons;
            form.Configuration.Yes = new string[] { "sim", "ok", "yes", "demoro" };
            form.Configuration.No = new string[] { "nao", "nops", "no", "errado" };
            form.Message("Por favor, responda as perguntas abaixo para agendar sua avaliação");
            form.OnCompletion(async (context, pedido) => {

                

                await context.PostAsync("Sua avaliação foi agendada. Entraremos em contato para realizar a confirmação.");
                Thread.Sleep(3000);

            });
            return form.Build();
        }

        public async Task StartAsync(IDialogContext context)
        {
            var myform = new FormDialog<Pedido>(new Pedido(), Pedido.BuildForm, FormOptions.PromptInStart, null);

            context.Call<Pedido>(myform, null);

            Thread.Sleep(2000);
        }

        public IList<Attachment> GetCardsAttachments()
        {
            throw new NotImplementedException();
        }

        public Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            throw new NotImplementedException();
        }

        public Task MessageReceivedAsyncCardAction(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            throw new NotImplementedException();
        }

        public Task MessageResumeAfter(IDialogContext context, IAwaitable<DialogResponse> result)
        {
            throw new NotImplementedException();
        }


    }
    
    public enum Modalidades
    {
        [Terms("Natacao","Natação","Nadar","Muaithai","MuaiThay","Muai thai", "Pilates", "Pillates", "Circuito","Cirtcuito Funcional")]
        [Describe("Natação")]
        Natacao = 1,
        MuayThai,
        Pilattes,
        Funcional

    }

    public enum Turno
    {
        [Prompt("Qual melhor horário para realizar sua avaliação?")]
        [Terms("Manhã","De manhã", "cedo", "De tarde", "Na parte da tarde", "Noturno", "De noite")]
        [Describe("Manhã")]
        Manha = 1,
        Tarde,
        Noite
    }

    public enum Professor
    {
        [Prompt("Qual professor fará a sua avaliação?")]
        [Terms("Gerson","Gersin", "Thiagao", "Dani", "Magela", "Jofirus", "Magelous")]
        Gerson = 1,
        Thiago,
        Daniel,
        Gustavo
    }

}