﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace poc_architecture_netfram_v2.Services
{
    public class Fichas
    {
        public string NomeExercicio { get; set; }
        public string QtdRepeticoes { get; set; }
        public string Peso { get; set; }
        public string UrlImagem { get; set; }
        public string ValorAcao { get; set; }
    }
}