﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace poc_architecture_netfram_v2.Services
{
    public class FichasAPIServices
    {
        public RestClient _ficha;

        public FichasAPIServices()
        {
            _ficha = new RestClient();
            _ficha.BaseUrl = new System.Uri("http://wsgymapi.azurewebsites.net/");
            //_ficha.BaseUrl = new System.Uri("http://localhost:57776/");
        }

        public List<Fichas> ListarExercicios()
        {
            List<Fichas> result = new List<Fichas>();
            var request = new RestRequest($"api/Fichas", Method.GET);

            var response = _ficha.Execute(request);

            try
            {
                IEnumerable<Fichas> chamados = JsonConvert.DeserializeObject<IEnumerable<Fichas>>(response.Content);
                foreach (var item in chamados)
                {
                    result.Add(item);
                }
            }
            catch (Exception ex)
            {

                throw new Exception("Mensagem: " + ex.Message + ex.InnerException.Message);
            }
            return result;
        }
    }
}