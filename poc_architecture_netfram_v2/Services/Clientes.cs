﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace poc_architecture_netfram_v2.Services
{
    public class Clientes
    {
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Modalidade { get; set; }
    }
}