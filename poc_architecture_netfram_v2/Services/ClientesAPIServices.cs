﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;



namespace poc_architecture_netfram_v2.Services
{
    [Serializable]
    public class ClientesAPIServices
    {
        public RestClient _client;

        public ClientesAPIServices()
        {
            _client = new RestClient();            
            _client.BaseUrl = new System.Uri("http://wsgymapi.azurewebsites.net/");
            //_client.BaseUrl = new System.Uri("http://localhost:57776/");
        }

        public List<Clientes> ListarModalidade()
        {
            List<Clientes> result = new List<Clientes>();
            var request = new RestRequest($"api/Clientes", Method.GET);

            var response = _client.Execute(request);

            try
            {
                Clientes chamados = JsonConvert.DeserializeObject<Clientes>(response.Content);
                result.Add(chamados);
            }
            catch (Exception ex)
            {

                throw new Exception("Mensagem: " + ex.Message + ex.InnerException.Message);
            }
            return result;
        }
    }
}