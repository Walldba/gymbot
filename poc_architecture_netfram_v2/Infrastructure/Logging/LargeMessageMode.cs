﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace poc_architecture_netfram_v2.Infrastructure.Logging
{
    public enum LargeMessageMode
    {
        Discard,
        Trim,
        Error
    }
}